import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import LoginRegister from './components/LoginRegister';
import MovieList from './components/MovieList';
import AppHeader from './components/AppHeader';
import { useDispatch } from 'react-redux';
import { getAuthSession } from './utils/auth';
import { loginAction } from './store/actions/auth';
import Profile from './components/Profile';

function App() {

  const dispatch = useDispatch();
  if (getAuthSession()) {
    dispatch(loginAction(getAuthSession()));
  }

  return (
    <BrowserRouter>
      <div className="App">

        <AppHeader />

        <Switch>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route path="/login">
            <LoginRegister />
          </Route>
          <Route path="/movies">
            <MovieList />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
