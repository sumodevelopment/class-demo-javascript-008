import React from 'react';
import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const withAuth = Component => props => {

    const auth = useSelector(state => state.auth);

    if ( auth.key ) {
        return <Component {...props} />
    } else {
        return <Redirect to="/login" />
    }
}

export default withAuth;