import { getStorage } from "./storage";

const STORAGE_KEY = 'user';

export const DEFAULT_AUTH_STATE = {
    username: '',
    key: false
};

export const getAuthSession = () => {
    const storedAuth = getStorage( STORAGE_KEY );
    if (!storedAuth) {
        return DEFAULT_AUTH_STATE;
    }
    return storedAuth;
}