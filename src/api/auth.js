export const userLogin = (username, password) => {
    return fetch( 'https://themovieboks.herokuapp.com/v1/users/login',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            user: {
                username, 
                password
            }
        })
    })
    .then(r => r.json())
    .then(r => {
        if (r.success === false) {
            throw Error( r.error );
        }
        return r;
    })
    .then(r => r.data)
}

export const userRegister = (username, password) => {
    return fetch( 'https://themovieboks.herokuapp.com/v1/users/register',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            user: {
                username, 
                password
            }
        })
    })
    .then(r => r.json())
    .then(r => {
        if (r.success === false) {
            throw Error( r.error );
        }
        return r;
    })
    .then(r => r.data)
}