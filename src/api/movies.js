// get movies

export const getMovies = () => {
    return fetch('https://themovieboks.herokuapp.com/v1/movies')
    .then(r => r.json())
    .then(r => r.data)
};