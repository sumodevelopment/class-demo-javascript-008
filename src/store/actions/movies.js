// Types
export const ACTION_SET_MOVIES = 'ACTION_SET_MOVIES';
export const ACTION_ADD_MOVIE = 'ACTION_ADD_MOVIES';

// Actions
export const setMoviesAction = (movies = []) => ({
    type: ACTION_SET_MOVIES,
    payload: movies
});

export const addMovieAction = (movie) => ({
    type: ACTION_ADD_MOVIE,
    payload: movie
});

