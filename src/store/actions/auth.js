export const ACTION_LOGIN = 'ACTION_LOGIN';
export const ACTION_LOGOUT = 'ACTION_LOGOUT';

export const loginAction = user => ({
    type: ACTION_LOGIN,
    payload: user
});

export const logoutAction = () => ({
    type: ACTION_LOGOUT
});