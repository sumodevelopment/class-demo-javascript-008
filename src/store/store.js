import { createStore, combineReducers } from 'redux';
import moviesReducer from './reducers/movies';
import authReducers from './reducers/auth';

const rootReducers = combineReducers({
    movies: moviesReducer,
    auth: authReducers
});

export default createStore( 
    rootReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);