import { ACTION_LOGIN, ACTION_LOGOUT } from '../actions/auth';
import { setStorage, removeStorage } from '../../utils/storage';
import { DEFAULT_AUTH_STATE } from '../../utils/auth';

const authReducers = (state = DEFAULT_AUTH_STATE, action) => {
    switch( action.type ) {
        case ACTION_LOGIN: 
            setStorage('user', action.payload);
            return action.payload;
        case ACTION_LOGOUT:
            removeStorage('user');
            return DEFAULT_AUTH_STATE;
        default: 
            return state;
    }
}

export default authReducers;