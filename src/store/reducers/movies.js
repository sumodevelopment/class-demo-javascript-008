import { ACTION_SET_MOVIES } from '../actions/movies';

const moviesReducer = (state = [], action) => {
    switch ( action.type ) {
        case ACTION_SET_MOVIES:
            return action.payload;
        default:
            return state;
    }
};

export default moviesReducer;