import React, { useEffect, useState } from 'react';
import './MovieList.css';
import { getMovies } from '../api/movies';
import MovieListSearchBar from './MovieListSearchbar';
import withAuth from '../hoc/withAuth';
import { useSelector, useDispatch } from 'react-redux';
import { setMoviesAction } from '../store/actions/movies';

const MovieList = (props) => {

    // Create a new state called movies.
    const movies = useSelector(state => state.movies);
    const dispatch = useDispatch();

    const [ searchText, setSearchText ] = useState('');
    const [ isLoading, setIsLoading ] = useState(true);

    // Executes when component renders
    useEffect(() => {
        getMovies().then(data => {
            dispatch( setMoviesAction( data ) );
        }).catch(e => {

        }).finally(() => {
            setIsLoading(false);
        });
    }, [ dispatch ]);

    const handleSearchChanged = searchText => setSearchText(searchText);

    // Map the list of movies to a list of JSX elements.
    const movieList = movies.filter(movie => {
        return movie.title.toLowerCase().includes( searchText.toLowerCase() );
    }).map(movie => {
        return (
            <div key={movie.id} className="movie">
                <div className="movie-cover">
                    <img src={movie.cover} alt={movie.title} />
                </div>
                <div className="movie-meta">
                    <h4 className="movie-title">{movie.title}</h4>
                    <p className="movie-description">{movie.description}</p>
                    <small className="movie-rating">{movie.rating}</small>
                </div>
            </div>
        );
    });

    return (
        <div className="container">
            <MovieListSearchBar onSearchChanged={ handleSearchChanged }/>
            <div className="movies">
                <h4 className="title">Movies: { movieList.length }</h4>
                { isLoading && <p>Loading movies...</p> }
                {movieList}
            </div>
        </div>
    )
}

export default withAuth(MovieList);