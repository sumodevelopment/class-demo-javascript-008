import React from 'react';
import withAuth from '../hoc/withAuth';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Profile = () => {

    const auth = useSelector(state => state.auth);
    const avatarUrl = `https://api.adorable.io/avatars/250/${auth.username}.png`;

    const profileStyles = {
        display: 'flex',
    };
    const avatarStyles = {
        marginRight: '1em'
    };

    return (
        <div className="container">
            <p>
                <Link to="/movies">Back</Link> 
            </p>
            <br/>
            <div style={profileStyles}>
                <div style={avatarStyles}>
                    <img src={avatarUrl} alt={auth.username} width="100" />
                </div>
                <div>
                    <h4>Welcome back,</h4>
                    <p>{auth.username}</p>
                </div>
            </div>
        </div>
    )
}

export default withAuth(Profile);