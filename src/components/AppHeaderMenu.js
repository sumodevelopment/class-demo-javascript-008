import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logoutAction } from '../store/actions/auth';
import './AppHeaderMenu.css';

const AppHeaderMenu = () => {

    const location = useLocation();
    const [showProfileButton, setShowProfileButton] = useState(true);

    useEffect(() => {
        if (location.pathname === '/profile') {
            setShowProfileButton(false);
        } else {
            setShowProfileButton(true);
        }
    }, [location])

    const dispatch = useDispatch();

    const onLogoutClicked = () => {
        if (window.confirm('Are you sure?')) {
            dispatch(logoutAction());
        }
    }

    return (
        <nav>
            <ul className="navbar">
                {showProfileButton &&
                    <li>
                        <Link to="profile">Profile</Link>
                    </li>
                }
                <li>
                    <button className="btn profile-button" onClick={onLogoutClicked}>Logout</button>
                </li>
            </ul>
        </nav>
    )
}

export default AppHeaderMenu;