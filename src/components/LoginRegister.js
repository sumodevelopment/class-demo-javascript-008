import React, { useState } from 'react';
import './LoginRegister.css';
import { userLogin, userRegister } from '../api/auth';
import { Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { loginAction } from '../store/actions/auth';

const LoginRegister = () => {

    const auth = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const [isLoading, setIsLoading] = useState(false);
    const [error, setError]         = useState('');
    const [username, setUsername]   = useState('');
    const [password, setPassword]   = useState('');

    const onUsernameChange = (e) => setUsername(e.target.value.trim());
    const onPasswordChange = (e) => setPassword(e.target.value.trim());

    const onLoginClicked = async () => {
        setIsLoading(true);
        setError('');
        let userId = -1;

        // Process a login.
        try {
            userId = await userLogin(username, password);
        } catch (e) {
            setError(e.message);
        } finally {
            setIsLoading(false);
            if (userId !== -1) {
                setAuthState(userId);
            }
        }
    }

    const onRegisterClicked = async () => {
        setIsLoading(true);
        setError('');
        let userId = -1;
        // Process a register.
        try {
            userId = await userRegister(username, password);
        } catch (e) {
            setError(e.message);
        } finally {
            console.log(userId);
            setIsLoading(false);
            if (userId !== -1) {
                setAuthState(userId);
            }
        }
    }

    const setAuthState = userId => {
        const user = { username, key: userId };
        dispatch( loginAction( user ) );
    }

    return (
        <div className="container">
            { auth.key && <Redirect to="/movies" /> }
            <form>
                <h1 className="title">Login or Register</h1>
                <input type="text" placeholder="username..." onChange={onUsernameChange} className="form-control" />
                <input type="password" placeholder="password..." onChange={onPasswordChange} className="form-control" />

                <button disabled={isLoading} onClick={onLoginClicked} className="btn" type="button">Login</button>
                <button disabled={isLoading} onClick={onRegisterClicked} className="btn" type="button">Register</button>
            </form>

            {isLoading && <p>Attempting...</p>}
            {error &&
                <p>{error}</p>
            }
        </div>
    );
};

export default LoginRegister;