import React from 'react';
import './AppHeaderSearchBar.css';

const MovieListSearchBar = (props) => {
    return (
        <div className="searchbar-container">
            <input 
                className="header-searchbar"
                type="text" 
                placeholder="Search for a movie..." 
                onChange={ (e) => props.onSearchChanged(e.target.value.trim()) } />
        </div>
    );
};

export default MovieListSearchBar;