import React from 'react';
import './AppHeader.css';
import { useSelector } from 'react-redux';
import AppHeaderMenu from './AppHeaderMenu';

const AppHeader = () => {

    const auth = useSelector(state => state.auth);

    return (
        <header className="app-header">
            <div className="container app-header-content">
                <h2>The Movie Box</h2>
                {auth.key &&
                    <AppHeaderMenu />
                }
            </div>
        </header>
    )
}

export default AppHeader;